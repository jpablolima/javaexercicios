/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demometa;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class NewClass {
    public static void main(String[] args) {
   int senhaMestre, senhaTentativa ;  
        double n1, n2, soma ;  
          
        Scanner captura = new Scanner( System.in ) ;  
          
        System.out.println( "Cadastre uma senha ( Apenas Números ): " ) ;  
        senhaMestre = captura.nextInt( ) ;  
          
        System.out.println( "Digite um número: " ) ;  
        n1 = captura.nextDouble( ) ;  
		
        System.out.println( "Digite outro número: " ) ;  
        n2 = captura.nextDouble( ) ;  
        soma = n1 + n2 ;  
          
        System.out.println( "Digite a sua senha para desbloquear a soma: " ) ;  
        senhaTentativa = captura.nextInt( ) ;  
          
        if( senhaTentativa == senhaMestre ){  
            System.out.println( "A soma é: "+soma ) ;  
        }else{  
            System.out.println( "Erro! Senha errada!" ) ;  
        }  
    } 
}
